#!/bin/bash

for IMAGE_NAME in $(find . -mindepth 1 -maxdepth 1 -type d | cut -c 3-) ; do
    echo "$IMAGE_NAME"
    cd $IMAGE_NAME/
    docker build --tag $IMAGE_PREFIX/$IMAGE_NAME:latest .
    docker push $IMAGE_PREFIX/$IMAGE_NAME:latest
    cd ../
done
